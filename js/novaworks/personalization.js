(function () {
    jQuery(document).ready(function () {
        var nav = jQuery('.nav_header');
        var messages = jQuery("#ajax_global_messages");
        var position = nav.position();

        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > position.top ) {
                nav.addClass("nav_header_fix");
                messages.addClass("ajax_global_messages_fixed");
            } else {
                nav.removeClass("nav_header_fix");
                messages.removeClass("ajax_global_messages_fixed");
            }

            jQuery(".product-item").each(function (index, value) {
                var columns = jQuery(value).find("[class*='col']:not(.animated)");

                if (columns.length > 0) {
                    var firstsection = columns.slice(0, 1);
                    var secondSection = columns.slice(1);
                    if (firstsection.length > 0)
                        checkAnimation(firstsection, "bounceInLeft");
                    if (secondSection.length > 0)
                        checkAnimation(secondSection, "bounceInRight")
                }
            });

        });

        var kindSize = window.getComputedStyle(document.body, ':before').getPropertyValue('content');
        if (kindSize === "tablet") {
            jQuery("img[data-medium]").each(function (index, value) {
                var $tag = jQuery(value);
                var urlImage = $tag.attr("data-medium");
                $tag.attr("src", urlImage);
            })
        }


    });

})();

function isElementInViewport(elem) {
    var $element = jQuery(elem);

    // Get the scroll position of the page.
    var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'body' : 'html');
    var viewportTop = jQuery(scrollElem).scrollTop();
    var viewportBottom = viewportTop + jQuery(window).height();

    try {
        // Get the position of the element on the page.
        var elemTop = Math.round($element.offset().top);
        var elemBottom = elemTop + $element.height();
    } catch (err) {
        console.log(err)
    }


    return ((elemTop < viewportBottom) && (elemBottom > viewportTop));
}

// Check if it's time to start the animation.
function checkAnimation(elem, effect) {
    var $toAnimate = $(elem);

    // If the animation has already been started
    if ($toAnimate.hasClass('animated ' + effect)) return;

    if (isElementInViewport($toAnimate)) {
        // Start the animation
        $toAnimate.addClass('animated ' + effect);
    }
}