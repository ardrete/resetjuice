<?php

try {
    require_once('app/Mage.php');
    Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
    $installer = new Mage_Sales_Model_Mysql4_Setup;


    $attribute = array(
        'type' => 'varchar',
        'input' => 'text',
        'backend_type' => 'varchar',
        'frontend_input' => 'text',
        'is_user_defined' => true,
        'frontend_class' => 'start_date_program',
        'label' => '¿Cuando quieres iniciar tu plan?',
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'searchable' => true,
        'filterable' => true,
        'comparable' => false,
        'default' => ' '
    );


    $installer->addAttribute('order', 'start_date_program', $attribute);
    $installer->addAttribute('quote', 'start_date_program', $attribute);


//    $installer->updateAttribute('order', 'start_date_program', 'required', false);
//    $installer->updateAttribute('quote', 'start_date_program', 'required', false);

    $installer->endSetup();
    echo "success";
} catch (Exception $e) {
    echo $e;
}





